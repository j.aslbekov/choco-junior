<?php

namespace App\Listeners;

use App\Events\UserCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GenerateQrCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        $userinfo = $event->user;
        $userPath = route('user.show', ['id' => $userinfo->id]);
        $image = QrCode::format('png')->generate($userPath);

        $qrName = time();
        $folder = date('Y-m-d');
        $output_file = "/images/user_qr/$folder/img-$qrName.png";

        Storage::put($output_file, $image);

        $userinfo->qr_path = $output_file;
        $userinfo->save();
    }
}
