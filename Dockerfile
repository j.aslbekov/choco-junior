FROM php:8.0-apache

RUN apt-get update -y \
    && apt-get install -y openssl zip unzip git libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install \
	pdo_mysql \
	&& a2enmod \
	rewrite

